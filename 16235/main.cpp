#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

typedef struct Tree{
	int r, c, age;
	Tree(){}
	Tree(int _r, int _c, int _age) : r(_r), c(_c), age(_age){}
	bool operator<(const Tree& a)const{
		return age < a.age;
	}
}Tree;

int N, M, K;
int Land[11][11];
int A[11][11];
int dir[8][2] = { { -1, 0 }, { -1, 1 }, { 0, 1 }, { 1, 1 }, { 1, 0 }, { 1, -1 }, { 0, -1 }, { -1, -1 } };
vector<Tree> tree;
vector<Tree> deadTree;

void spring(void){
	vector<Tree> temp;
	sort(tree.begin(), tree.end());

	
	for(int i = 0; i < tree.size(); ++i){
		int r = tree[i].r;
		int c = tree[i].c;
		int age = tree[i].age;
		if (age > Land[r][c]) {
			deadTree.push_back(tree[i]);
		}
		else {
			Land[r][c] -= age;
			++tree[i].age;
			temp.push_back(tree[i]);
		}
	}

	tree.clear();
	for (int i = 0; i < temp.size(); ++i)
		tree.push_back(temp[i]);
}

void summer(void){
	for(int i = 0; i < deadTree.size(); ++i){
		Tree t = deadTree[i];
		Land[t.r][t.c] += (t.age / 2);
	}
	deadTree.clear();
}

bool isRightDir(int r, int c){
	return r > 0 && r <= N && c > 0 && c <= N;
}

void fall(void){
	int size = tree.size();
	for (int i = 0; i < size; ++i){
		if (tree[i].age % 5 == 0){
			for (int j = 0; j < 8; ++j){
				int r = tree[i].r + dir[j][0];
				int c = tree[i].c + dir[j][1];
				if (isRightDir(r, c)) tree.push_back(Tree(r, c, 1));
			}
		}
	}
}

void winter(void){
	for (int i = 1; i <= N; ++i)
		for (int j = 1; j <= N; ++j)
			Land[i][j] += A[i][j];
}

void simulate(void){
	spring();
	summer();
	fall();
	winter();
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> M >> K;
	for (int i = 1; i <= N; ++i){
		for (int j = 1; j <= N; ++j){
			cin >> A[i][j];
			Land[i][j] = 5;
		}
	}

	for (int i = 0; i < M; ++i){
		int a, b, c;
		cin >> a >> b >> c;
		tree.push_back(Tree(a, b, c));
	}

	for (int i = 0; i < K; ++i) simulate();

	cout << tree.size() << '\n';

	return 0;
}
