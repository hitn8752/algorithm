#include<iostream>
#include<queue>

using namespace std;

typedef struct GRN{
	int seed, a, b;
	GRN(int _a, int _b) : seed(1983), a(_a), b(_b) {};
	int next(void){
		int ret = seed;
		seed = ((seed * (long long)a) + b) % 20090711;
		return ret;
	}
};

typedef struct {
	bool operator()(int a, int b){
		return a > b;
	}
}compare;

int C, N;
priority_queue<int, vector<int>, compare> min_heap;
priority_queue<int, vector<int>, less<int>> max_heap;

int solve(int n, GRN grn){
	int ret = 0;

	for (int i = 0; i < n; ++i){
		if (min_heap.size() == max_heap.size())
			max_heap.push(grn.next());
		else
			min_heap.push(grn.next());

		if (!min_heap.empty() && !max_heap.empty() && 
			min_heap.top() < max_heap.top()){
			int a = min_heap.top(), b = max_heap.top();
			min_heap.pop(); max_heap.pop();

			min_heap.push(b); max_heap.push(a);
		}
		ret = (ret + max_heap.top()) % 20090711;
	}
	return ret;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 0; test < C; ++test){
		while (!min_heap.empty()) min_heap.pop();
		while (!max_heap.empty()) max_heap.pop();

		int a, b;
		cin >> N >> a >> b;
		int result = solve(N, GRN(a, b));

		cout << result << '\n';
	}

	return 0;
}