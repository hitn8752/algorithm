#include<iostream>

using namespace std;

int T;

int gcd(int a, int b){
	if (a < b) return gcd(b, a);

	if (a % b == 0) return b;
	return gcd(b, a%b);
}

int main(void){
	
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> T;
	for (int test = 1; test <= T; ++test){
		int a, b, result;
		cin >> a >> b;
		result = a*b/gcd(a, b);
		cout << result << '\n';
	}

	return 0;
}