#include<iostream>
#include<vector>

using namespace std;

int N, M;
vector<int> indegree;
vector<int> adj[32001];

int delZeroDegree(void){
	for (int i = 1; i < indegree.size(); ++i){
		if (indegree[i] == 0){
			indegree[i] = -1;
			for (int j = 0; j < adj[i].size(); ++j)
				indegree[adj[i][j]]--;
			return i;
		}
	}
	return -1;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);
	
	cin >> N >> M;
	indegree = vector<int>(N + 1);
	for (int i = 0; i < N; ++i) adj[i].clear();
	for (int i = 0; i < M; ++i){
		int a, b;
		cin >> a >> b;
		adj[a].push_back(b);
		indegree[b]++;
	}

	for (int i = 0; i < N; ++i){
		int order = delZeroDegree();
		cout << order << " ";
	}
			
	return 0;
}