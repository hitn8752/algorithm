#include<iostream>

#define MOD 1000000007

using namespace std;

int C, N;
int cache[101];

int tiling(int width) {
	
	if (width <= 1) return 1;

	int& ret = cache[width];
	if (ret != -1) return ret;

	return ret = (tiling(width - 1) + tiling(width - 2)) % MOD;
}

int asymtiling(int width) {

	if (width % 2 == 1)
		return (tiling(width) - tiling(width / 2) + MOD) % MOD;
	
	int ret = tiling(width);
	ret = (ret - tiling(width / 2) + MOD) % MOD;
	ret = (ret - tiling(width / 2 - 1) + MOD) % MOD;
	return ret;
}

void init(void){
	cin >> N;
	for (int i = 0; i < 101; ++i)
		cache[i] = -1;

}

int main(void) {
	
	freopen("./input.txt", "r", stdin);
	cin >> C;

	for (int test = 0; test < C; ++test) {
		init();
		int ret = asymtiling(N);

		cout << ret << '\n';
	}

	return 0;
}