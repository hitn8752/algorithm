#include<iostream>
#include<string.h>

#define MAX(x, y) (x > y) ? x : y

using namespace std;

int C, N;
int triangle[100][100];
int cache[100][100];

int solve(int x, int y, int sum) {

	if (y == N - 1) return sum + triangle[y][x];

	sum += triangle[y][x];

	return MAX(solve(x, y + 1, sum), solve(x + 1, y + 1, sum));

}

int solve2(int x, int y) {

	if (y == N - 1) return triangle[y][x];

	int& ret = cache[y][x];
	if (ret != -1) return ret;

	int a = solve2(x, y + 1);
	int b = solve2(x + 1, y + 1);

	return ret = (MAX(a, b)) + triangle[y][x];
}

int main(void) {
	
	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test) {
		cin >> N;
		for (int i = 0; i < N; ++i){
			for (int j = 0; j <= i; ++j){
				int a;
				cin >> a;
				triangle[i][j] = a;
				cache[i][j] = -1;
			}
		}

		memset(cache, -1, sizeof(cache));

		int ret = solve2(0, 0);
		//int ret = solve(0, 0, 0);

		cout << ret << endl;
	}


	return 0;
}