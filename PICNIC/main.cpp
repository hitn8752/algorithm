//
//  main.cpp
//  PICNIC
//
//  Created by Hwang on 2018. 11. 16..
//  Copyright © 2018년 Hwang. All rights reserved.
//

#include <iostream>

using namespace std;

bool relation[10][10];

bool taken[10];

int t, student, disjoin;

void init() {
    for(int i = 0; i < 10; ++i) {
        for(int j = 0; j < 10; ++j) {
            relation[i][j] = false;
        }
        taken[i] = false;
    }
    
    cin >> student >> disjoin;
    
    int a, b;
    for(int i = 0; i < disjoin; ++i) {
        cin >> a >> b;
        relation[a][b] = true;
        relation[b][a] = true;
    }
    
}

int countPairings(bool taken[10], int index) {
    
    int ret = 0;
    
    bool finished  = true;
    
    for(int i = 0; i < student; ++i) if(!taken[i]) finished = false;
    if(finished) {
        //cout << endl;
        return 1;
    }
    
    for(int i = index; i < student; ++i) {
        for(int j = index + 1; j < student; ++j) {
            if(!taken[i] && !taken[j] && relation[i][j] && i < j) {
                taken[i] = taken[j] = true;
                //cout << "( " << i << ", " << j << " ) ";
                ret += countPairings(taken, i + 1);
                taken[i] = taken[j] = false;
            }
        }
    }
    
    return ret;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    
    freopen("/Users/hwang/Documents/Xcodeworkspace/PICNIC/PICNIC/input.txt", "r", stdin);
    
    cin >> t;
    
    for(int test = 0; test < t; ++test){
        init();
        
        int ret = countPairings(taken, 0);
        
        cout << ret << endl;
    }
    
    return 0;
}
