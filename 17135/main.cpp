#include<iostream>
#include<algorithm>
#include<cstring>
#include<vector>
#include<queue>

using namespace std;

typedef struct Enermy{
	int r, c, d;
	bool operator<(const Enermy& a)const {
		return (d == a.d) ? c > a.c : d > a.d;
	}
}Enermy;

int N, M, D, A, ans;
int map[15][15];
int mCopy[15][15];
int ach[5];
int dir[4][2] = { { 0, -1 }, { -1, 0 }, { 0, 1 }, { 1, 0 } };

void init(void){
	cin >> N >> M >> D;
	for (int i = 0; i < N; ++i){
		for (int j = 0; j < M; ++j){
			cin >> map[i][j];
		}
	}

	memset(ach, 0, sizeof(ach));
	memset(mCopy, 0, sizeof(mCopy));
	ans = 0; A = 0;
}

bool isRightDir(int r, int c, int h, int w){
	return r >= 0 && r < h && c >= 0 && c < w;
}

void bfs(vector<pair<int, int>>& v, int p, int h){
	
	int visited[15][15];
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j)
			visited[i][j] = false;

	queue<Enermy> q;
	priority_queue<Enermy> pq;

	//���� ��ġ
	q.push({ h, p, 0 });

	while (!q.empty()){
		Enermy cur = q.front(); q.pop();
		
		for (int d = 0; d < 4; ++d){
			int nr = cur.r + dir[d][0];
			int nc = cur.c + dir[d][1];
			if (!isRightDir(nr, nc, h, M) || visited[nr][nc] || cur.d + 1 > D) continue;
			q.push({ nr, nc, cur.d + 1 });
			visited[nr][nc] = true;
			if (mCopy[nr][nc] == 1){
				pq.push({ nr, nc, cur.d + 1 });
			}
		}
	}

	if (!pq.empty())
		v.push_back({ pq.top().r, pq.top().c });

}

int shoot(int h){

	int ret = 0;
	vector<pair<int, int>> v;
	for (int i = 0; i < A; ++i) bfs(v, ach[i], h);
	
	for (int i = 0; i < v.size(); ++i){
		pair<int, int>& p = v[i];
		if ((--mCopy[p.first][p.second]) < 0)
			mCopy[p.first][p.second] = 0;
		else
			ret++;
	}

	return ret;
}

int simulate(void){

	memcpy(mCopy, map, sizeof(mCopy));

	int ret = 0;

	int cHeight = N;

	while (cHeight > 0){
		int killedEnermy = shoot(cHeight);
		ret += killedEnermy;
		cHeight--;
	}

	return ret;
}

void putAch(int start, int n){

	if (n == 3){
		ans = max(ans, simulate());
		return;
	}

	for (int i = start; i < M; ++i){
		ach[A++] = i;
		putAch(i + 1, n + 1);
		ach[--A] = -1;
	}
}

int main(void){

	freopen("./input.txt", "r", stdin);

	for (int i = 0; i < 6; ++i){
		init();

		putAch(0, 0);

		cout << ans << '\n';
	}
	

	return 0;
}