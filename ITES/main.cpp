#include<iostream>
#include<queue>
#include<cmath>

using namespace std;

int C;
unsigned A[2];
queue<int> q;

int main(void) {

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 1; test <= C; ++test){
		int k, n, sum = 0, cnt = 0;
		cin >> k >> n;
		
		while (!q.empty()) q.pop();

		A[0] = 1983;
		for (int i = 1; i <= n; ++i){
			A[1] = A[0] * 214013 + 2531011;
			int signal = A[0] % 10000 + 1;
			q.push(signal);

			sum += signal;

			while (sum > k) {
				sum -= q.front();
				q.pop();
			}

			if (sum == k) cnt++;

			A[0] = A[1];
		}
		cout << cnt << '\n';
	}

	return 0;
}