#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

typedef struct TreeNode{
	TreeNode* child[100];
	int c_size, x, y, r;
	TreeNode(){}
	TreeNode(int x, int y, int r) : c_size(0), x(x), y(y),r(r){
		for (int i = 0; i < 100; ++i)
			child[i] = NULL;
	}
	~TreeNode(){
		for (int i = 0; i < 100; ++i)
			delete child[i];
	}
	void append(TreeNode* a){
		child[c_size++] = a;
	}
};

typedef struct Node{
	int x, y, r;
	Node(){}
	Node(int x, int y, int r) : x(x), y(y), r(r) {}
};

int C, N;
vector<Node> v;

int sqr(int a){
	return a*a;
}

int sqrdis(int a, int b){
	return sqr(v[a].y - v[b].y) + sqr(v[a].x - v[b].x);
}

bool encloses(int a, int b){
	return (v[a].r > v[b].r) && (sqrdis(a, b) < sqr(v[a].r - v[b].r));
}

bool isChild(int parent, int child){
	if (!encloses(parent, child)) return false;
	for (int i = 0; i < N; ++i){
		if (i != parent && i != child && encloses(parent, i) && encloses(i, child))
			return false;
	}

	return true;
}
TreeNode* getTree(int root){
	Node a = v[root];
	TreeNode* ret = new TreeNode(a.x, a.y, a.r);

	for (int ch = 0; ch < N; ++ch){
		if (isChild(root, ch))
			ret->append(getTree(ch));
	}

	return ret;
}

int longest;

int height(TreeNode* root) {
	vector<int> heights;
	for (int i = 0; i < root->c_size; ++i)
		heights.push_back(height(root->child[i]));

	if (heights.empty()) return 0;

	sort(heights.begin(), heights.end());

	if (heights.size() >= 2)
		longest = max(longest, 2 + heights[heights.size() - 2] +
		heights[heights.size() - 1]);

	return heights.back() + 1;

}
int solve(TreeNode* root){
	longest = 0;
	int h = height(root);
	return max(longest, h);
}

int main(void) {
	
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 0; test < C; ++test){
		v.clear();
		cin >> N;
		for (int i = 0; i < N; ++i){
			int x, y, r;
			cin >> x >> y >> r;
			v.push_back(Node(x, y, r));
		}

		TreeNode* root = getTree(0);

		int result = solve(root);

		cout << result << '\n';

		delete root;
	}
	return 0;
}