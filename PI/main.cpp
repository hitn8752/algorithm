#include<iostream>
#include<string>
#include<string.h>

#define MIN(x, y) (x < y) ? x : y
#define REF 987654321

using namespace std;

int C;
int cache[10002];
string str;

//[start....end]
int cal(int start, int end) {

	string a = str.substr(start, end - start + 1);
	if (a == string(a.size(), a[0])) return 1;

	bool isProgression = true;
	for (int i = 0; i < a.size() - 1; ++i)
		if (a[i + 1] - a[i] != a[1] - a[0]) 
			isProgression = false;
	if (isProgression && ((a[1] - a[0] == 1) || (a[1] - a[0] == -1)))
		return 2;

	bool isOk = true;
	for (int i = 0; i < a.size(); ++i){
		if (a[i] != a[i % 2])
			isOk = false;
	}

	if (isOk) return 4;
	if (isProgression) return 5;

	return 10;
}

int solve(int start) {
	
	if (start >= str.size()) return 0;

	int& ret = cache[start];
	if (ret != -1) return ret;

	ret = REF;
	for (int i = 3; i <= 5; ++i) {
		if (start + i <= str.size()){
			int temp = cal(start, start + i - 1);
			int temp2 = solve(start + i);
			ret = MIN(ret, (temp + temp2));
		}
	}

	return ret;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test) {
		memset(cache, -1, sizeof(cache));
		cin >> str;
		int ret = solve(0);

		cout << ret << '\n';
	}

	return 0;
}