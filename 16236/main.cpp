#include<iostream>
#include<queue>

using namespace std;

typedef struct shark{
	int r, c, size, eat;
	shark(){}
	shark(int _r, int _c) : r(_r), c(_c), size(2), eat(0){}
	void eat_fish(){
		if (++eat == size){
			eat = 0;
			size++;
		}
	}
};

typedef struct cord{
	int r, c, time;
	cord(int _r, int _c, int _time) : r(_r), c(_c), time(_time){}
	bool operator<(const cord& a)const{
		return (r == a.r) ? (c > a.c) : (r > a.r);
	}
};

int N, Time;
int space[20][20];
bool visited[20][20];
int dir[4][2] = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };
shark s;

bool isRightDir(int r, int c){
	return (r >= 0 && r < N) && (c >= 0 && c < N);
}

bool bfs(void){
	for (int i = 0; i < N; ++i) for (int j = 0; j < N; ++j) visited[i][j] = false;

	queue<cord> q;
	priority_queue<cord> pq;
	visited[s.r][s.c] = true;
	q.push(cord(s.r, s.c, 0));
	
	while (!q.empty()){
		cord cur = q.front(); q.pop();
		int cr = cur.r;
		int cc = cur.c;

		if (space[cr][cc] > 0 && space[cr][cc] < s.size){
			if (pq.empty() || pq.top().time >= cur.time)
				pq.push(cord(cr, cc, cur.time));
		}

		for (int i = 0; i < 4; ++i){
			int nr = cr + dir[i][0];
			int nc = cc + dir[i][1];
			if (isRightDir(nr, nc) && !visited[nr][nc] && space[nr][nc] <= s.size){
				visited[nr][nc] = true;
				q.push(cord(nr, nc, cur.time + 1));
			}
		}
	}
	
	if (pq.empty()) return false;

	s.r = pq.top().r;
	s.c = pq.top().c;
	s.eat_fish();
	space[s.r][s.c] = 0;
	Time += pq.top().time;
	
	return true;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt","r",stdin);

	cin >> N;
	for (int i = 0; i < N; ++i){
		for (int j = 0; j < N; ++j){
			cin >> space[i][j];
			if (space[i][j] == 9){
				s = shark(i, j);
				space[i][j] = 0;
			}
		}
	}

	Time = 0;
	while (bfs());
	cout << Time << '\n';
	return 0;
}