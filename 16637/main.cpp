#include <iostream>
#include <vector>

#define REF -987654321
#define MAX(x, y) ( x > y ) ? x : y

using namespace std;

int n;
vector<char> eq;

void init(void) {

	char str[20];

	cin >> n;

	cin >> str;

	eq.push_back('\0');
	for (int i = 0; i < n; ++i) {
		eq.push_back(str[i]);
		eq.push_back('\0');
	}
}

void printEQ(void) {

	vector<char>::iterator iter;

	for (iter = eq.begin(); iter != eq.end(); ++iter){
		cout << *iter;
	}

	cout << endl;
}

int operate(int a, int b, char op) {

	int result = 0;

	switch (op) {
	case '+':
		result = a + b;
		break;
	case '-':
		result = a - b;
		break;
	case '*':
		result = a * b;
		break;
	default:
		break;
	}

	return result;
}

int calBracket(vector<char>& eq, int start, int end) {
	if (start + 2 == end) return eq[start + 1] - '0';

	int result = operate(eq[start + 1] - '0', eq[start + 5] - '0', eq[start + 3]);

	return result;
}

int calEq(vector<char>& eq) {
	
	int result = 0;
	vector<char> ops;

	for (int i = 0; i < eq.size(); ++i) {
		if (eq[i] == '(') {
			int end = i;
			while (eq[end] != ')') ++end;
			if (ops.empty())
				result = calBracket(eq, i, end);
			else {
				result = operate(result, calBracket(eq, i, end), ops[0]);
				ops.pop_back();
			}
			i = end;
		}
		else ops.push_back(eq[i]);
	}

	return result;
}

int solve(vector<char>& eq, int index) {

	if (index > eq.size()) {
		//printEQ();
		int a = calEq(eq);
		//cout << a << endl;
		return a;
	}

	int result = REF;

	eq[index] = '(';
	for (int i = index + 2; (i <= index + 6 ) && i <= eq.size(); i += 4) {
		eq[i] = ')';
		int temp = solve(eq, i + 2);
		result = MAX(result, temp);
		eq[i] = '\0';
	}
	eq[index] = '\0';

	return result;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	init();

	int result = solve(eq, 0);

	cout << result << endl;

	return 0;
}
