#include<iostream>
#include<map>

using namespace std;

int C, N;
map<int, int> cords;

bool isDominated(int p, int q){
	map<int, int>::iterator it = cords.lower_bound(p);

	if (it == cords.end()) return false;


	return q < it->second;
}

void removeDominated(int p, int q){
	map<int, int>::iterator it = cords.lower_bound(p);

	if (it == cords.begin()) return;
	--it;

	while (true){
		if (it->second > q) break;
		if (it == cords.begin()){
			cords.erase(it);
			break;
		}
		else{
			map<int, int>::iterator jt = it;
			--jt;
			cords.erase(it);
			it = jt;
		}
	} 
}

int registered(int p, int q){

	if (isDominated(p, q)) return cords.size();

	removeDominated(p, q);
	cords[p] = q;
	return cords.size();
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 1; test <= C; ++test){
		cords.clear();
		int result = 0;
		cin >> N;
		for (int i = 0; i < N; ++i){
			int p, q;
			cin >> p >> q;
			result += registered(p, q);
		}
		cout << result << '\n';
	}
	
	return 0;
}