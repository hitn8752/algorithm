#include<iostream>

using namespace std;

typedef struct Dice{
	int side[7];
	int r, c;
	Dice(){}
	Dice(int _r, int _c) :r(_r), c(_c){
		for (int i = 0; i <= 7; ++i) side[i] = 0;
	}
	int& operator[](int idx){
		return side[idx];
	}
	int top(void){
		return side[1];
	}
	int& down(void){
		return side[6];
	}
	void R(void){
		int temp = side[1];
		side[1] = side[4];
		side[4] = side[6];
		side[6] = side[3];
		side[3] = temp;
	}
	void F(void){
		int temp = side[1];
		side[1] = side[2];
		side[2] = side[6];
		side[6] = side[5];
		side[5] = temp;
	}
	void B(void){
		F();
		F();
		F();
	}
	void L(void){
		R();
		R();
		R();
	}
}Dice;

int N, M, X, Y, K;
int plate[21][21];
int dir[5][2] = { {0,0}, {0,1}, {0,-1}, {-1,0}, {1,0} };
Dice dice;

void roll(int d){
	if (d == 1) dice.R();
	else if (d == 2) dice.L();
	else if (d == 3) dice.B();
	else dice.F();
}

bool isRightDir(int r, int c){
	return r >= 0 && r < N && c >= 0 && c < M;
}

void solve(int d){
	int nr = dice.r + dir[d][0];
	int nc = dice.c + dir[d][1];

	if (isRightDir(nr, nc)) {
		roll(d);
		dice.r = nr;
		dice.c = nc;
		if (plate[nr][nc] == 0) plate[nr][nc] = dice.down();
		else {
			dice[6] = plate[nr][nc];
			plate[nr][nc] = 0;
		}
		cout << dice.top() << '\n';
	}
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> M >> X >> Y >> K;
	for (int i = 0; i < N; ++i){
		for (int j = 0; j < M; ++j){
			cin >> plate[i][j];
		}
	}

	dice = Dice(X, Y);
	for (int i = 0; i < K; ++i){
		int a; cin >> a;
		solve(a);
	}
	return 0;
}