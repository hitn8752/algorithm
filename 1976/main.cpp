#include<iostream>

using namespace std;

int N, M;
int root[201];

int find(int node){
	if (root[node] == node) return node;
	return root[node] = find(root[node]);
}

void join(int a, int b){
	int pa = find(a);
	int pb = find(b);
	root[pb] = pa;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> M;
	for (int i = 1; i <= N; ++i) root[i] = i;
	for (int i = 1; i <= N; ++i)
		for (int j = 1; j <= N; ++j){
			int a;
			cin >> a;
			if (a == 1) join(i, j);
		}


	int a; cin >> a;
	int p = find(a);
	for (int i = 1; i < M; ++i){
		cin >> a;
		int temp = find(a);
		if (p != temp) {
			cout << "NO\n";
			return 0;
		}
		p = temp;
	}

	cout << "YES\n";
	return 0;
}