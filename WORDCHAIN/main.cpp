#include<iostream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;

int C, N;
int adj[26][26];
int indegree[26], outdegree[26];
vector<string> graph[26][26];
vector<string> word;

void make_graph(void){
	for (int i = 0; i < 26; ++i){
		for (int j = 0; j < 26; ++j){
			adj[i][j] = 0;
			graph[i][j].clear();
		}
		indegree[i] = outdegree[i] = 0;
	}

	for (int i = 0; i < word.size(); ++i){
		int a = word[i][0] - 'a';
		int b = word[i][word[i].size() - 1] - 'a';
		adj[a][b]++;
		graph[a][b].push_back(word[i]);
		indegree[b]++; outdegree[a]++;
	}
}

bool checkEuler(void) {
	
	int plus1 = 0, minus1 = 0;
	for (int i = 0; i < 26; ++i){
		int delta = outdegree[i] - indegree[i];
		if (delta < -1 || 1 < delta) return false;
		if (delta == 1) plus1++;
		if (delta == -1) minus1++;
	}
	return (plus1 == 1 && minus1 == 1) || (plus1 == 0 && minus1 == 0);
}

void getEulerCircuit(int here, vector<int>& circuit){
	for (int there = 0; there < 26; ++there){
		while (adj[here][there] > 0){
			adj[here][there]--;
			getEulerCircuit(there, circuit);
		}
	}
	circuit.push_back(here);
}

vector<int> getEuler(void){
	vector<int> circuit;
	for (int i = 0; i < 26; ++i){
		if (outdegree[i] == indegree[i] + 1){
			getEulerCircuit(i, circuit);
			return circuit;
		} 
	}
	
	for (int i = 0; i < 26; ++i){
		if (outdegree[i]){
			getEulerCircuit(i, circuit);
			return circuit;
		}
	}
	return circuit;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 0; test < C; ++test){
		cin >> N;
		word.clear();
		for (int i = 0; i < N; ++i){
			char str[11];
			cin >> str;
			word.push_back(str);
		}

		make_graph();

		if (!checkEuler()) {
			cout << "IMPOSSIBLE\n";
			continue;
		}

		vector<int> cycle = getEuler();

		if (cycle.size() != word.size() + 1){
			cout << "IMPOSSIBLE\n";
			continue;
		}

		reverse(cycle.begin(), cycle.end());
		for (int i = 0; i < cycle.size() - 1; ++i){
			int a = cycle[i], b = cycle[i + 1];
			cout << graph[a][b].back() << " ";
			graph[a][b].pop_back();
		}
		cout << '\n';

	}
	return 0;
}