#include<iostream>


using namespace std;

int C, N;
int cache[101][101];
const int MOD = 10 * 1000 * 1000;

int poly(int n, int first) {
	
	if (n == first) return 1;

	int& ret = cache[n][first];
	if (ret != -1) return ret;

	ret = 0;
	for (int second = 1; second <= n - first; ++second) {
		int add = first + second - 1;
		add *= poly(n - first, second);
		add %= (MOD);
		ret += add;
		ret %= (MOD);
	}

	return ret;
}

int main(void) {
	
	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test) {
		cin >> N;
		memset(cache, -1, sizeof(cache));

		int ret = 0;
		for (int i = 1; i <= N; ++i) {
			ret += poly(N, i);
		}

		cout << ret % MOD<< '\n';
	}

	return 0;
}