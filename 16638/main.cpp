#include<iostream>
#include<vector>

#define REF -987654321
#define MAX(x, y) (x > y) ? x : y

using namespace std;

int n;
vector<char> eq;

void init() {
	
	cin >> n;

	char str[20]; 

	cin >> str;

	eq.push_back('\0');
	for (int i = 0; i < n; ++i) {
		eq.push_back(str[i]); 
		eq.push_back('\0');
	}
}

void printEq(void) {
	vector<char>::iterator iter;

	for (iter = eq.begin(); iter != eq.end(); ++iter) {
		cout << *iter;
	}
	cout << endl;
}

int operate(int a, int b, char op) {

	int result = 0;

	switch (op) {
	case '+':
		result = a + b;
		break;
	case '-':
		result = a - b;
		break;
	case '*':
		result = a * b;
		break;
	default:
		break;
	}

	return result;
}

int calBracket(vector<char>& eq, int start, int end) {

	if (start + 2 == end) return eq[start + 1] -'0';

	return operate(eq[start + 1] - '0', eq[start + 5] - '0', eq[start + 3]);
}

int ranking(char a) {
	
	switch (a) {
	case '+':
	case '-':
		return 1;
		break;
	case '*':
		return 2;
		break;
	default:
		break;
	}

	return 0;
}

int calEq(vector<char>& eq) {

	vector<char> op;
	vector<int> num;

	for (int start = 0; start < eq.size(); ++start) {
		if (eq[start] == '(') {
			int end = start;
			while (eq[end] != ')') ++end;
			int temp = calBracket(eq, start, end);
			num.push_back(temp);
			start = end;
		}
		else {
			while (!op.empty() && ranking(op.back()) > ranking(eq[start])) {
				int b = num.back();
				num.pop_back();
				int a = num.back();
				num.pop_back();
				char c = op.back();
				op.pop_back();
				num.push_back(operate(a, b, c));
			}
			op.push_back(eq[start]);
		}
	}
	
	/*int result = num[0];
	for (int i = 0; i < op.size(); ++i) {
		result = operate(result, num[i + 1], op[i]);
	}*/

	while (!op.empty()) {
		int b = num.back();
		num.pop_back();
		int a = num.back();
		num.pop_back();
		char c = op.back();
		op.pop_back();
		if (!op.empty() && c == '-' && op.back() == '-') {
			c = '+';
		}
		else if (!op.empty() && c == '+' && op.back() == '-') {
			c = '-';
		}
		num.push_back(operate(a, b, c));
	}

	return num.back();;
}

int solve(vector<char>& eq, int index) {
	
	if (index > eq.size()) {
		//printEq();
		int a = calEq(eq);
		//cout << a << endl;
		return a;
	}

	int result = REF;

	eq[index] = '(';
	for (int i = index + 2; i <= index + 6 && i < eq.size(); i += 4) {
		eq[i] = ')';
		int temp = solve(eq, i + 2);
		result = MAX(result, temp);
		eq[i] = '\0';
	}
	eq[index] = '\0';

	return result;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	init();

	cout << solve(eq, 0) << endl;

	return 0;
}
