#include<iostream>
#include<vector>

#define MAX(x, y) (x > y) ? x : y

using namespace std;

int C, N;
int S[501];
int cache[501];

int solve(int start) {

	int& ret = cache[start];
	if (ret != -1) return ret;

	ret = 1;
	for (int next = start + 1; next < N; ++next){
		if (S[next] > S[start]){
			int temp = solve(next) + 1;
			ret = MAX(ret, temp);
		}
	}

	return ret;
}
int main(void) {
	
	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test) {
		cin >> N;
		int a;
		for (int i = 0; i < N; ++i){
			cin >> a;
			S[i] = a;
		}

		memset(cache, -1, sizeof(cache));

		int ret = 0;
		for (int i = 0; i < N; ++i){
			int temp = solve(i);
			ret = MAX(ret, temp);
		}

		cout << ret << '\n';
	}

	return 0;
}