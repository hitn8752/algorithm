#include<iostream>
#include<string.h>

#define MOD 1000000007

using namespace std;

int C, N;
int cache[101];

int tiling(int width) {
	
	if (width <= 1) return 1;

	int& ret = cache[width];
	if (ret != -1) return ret;

	return ret = (tiling(width - 1) + tiling(width - 2)) % MOD;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test){
		cin >> N;

		memset(cache, -1, sizeof(cache));

		int ret = tiling(N);

		cout << ret << '\n';
	}

	return 0;
}