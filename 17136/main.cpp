#include<iostream>
#include<algorithm>

#define N 10
using namespace std;

int map[N][N];
int type[5][2] = { { 4, 4 }, { 3, 3 }, { 2, 2 }, { 1, 1 }, { 0, 0 } };
int nType[5] = { 5, 5, 5, 5, 5 };
int ans;

void init(void){
	for (int i = 0; i < N; ++i){
		for (int j = 0; j < N; ++j){
			cin >> map[i][j];
		}
	}
	ans = 500;
}

inline bool isRightDir(int r, int c){
	return r >= 0 && r < N && c >= 0 && c < N;
}

bool putBlock(int x, int y, int d, int op){
	
	bool ret = true;

	int dy = y + type[d][0];
	int dx = x + type[d][1];

	for (int i = y; i <= dy; ++i){
		for (int j = x; j <= dx; ++j){
			if (!isRightDir(i, j)) ret = false;
			else if ((map[i][j] += op) < 0) ret = false;
		}
	}

	return ret;
}

void dfs(int depth){

	if (depth > ans) return;

	int dx = -1, dy = -1;
	for (int i = 0; i < N; ++i){
		for (int j = 0; j < N; ++j){
			if (map[i][j] == 1){
				dy = i;
				dx = j;
				break;
			}
		}
		if (dy != -1) break;
	}

	if (dy == -1){
		ans = min(ans, depth);
		return;
	}

	for (int d = 0; d < 5; ++d){
		if (nType[d] == 0) continue;
		if (putBlock(dx, dy, d, -1)){
			nType[d]--;
			dfs(depth + 1);
			nType[d]++;
		}
		putBlock(dx, dy, d, 1);
	}
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	//for (int i = 0; i < 8; ++i){
	init();

	dfs(0);

	if (ans == 500) cout << "-1\n";
	else cout << ans << '\n';
	//}
	return 0;
}