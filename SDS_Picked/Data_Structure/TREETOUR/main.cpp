#include<iostream>

using namespace std;

typedef struct{
	int val, parent, left, right;
}TreeNode_t;

int N;
TreeNode_t node[27];

void preOder(int n) {
	if (n == -1) return;

	cout << (char)node[n].val;
	preOder(node[n].left);
	preOder(node[n].right);
}

void inOder(int n) {
	if (n == -1) return;

	inOder(node[n].left);
	cout << (char)node[n].val;
	inOder(node[n].right);
}

void postOder(int n) {

	if (n == -1) return;

	postOder(node[n].left);
	postOder(node[n].right);
	cout << (char)node[n].val;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> N;
	for (int i = 0; i < 26; ++i){
		node[i].val = i + 'A';
		node[i].parent = node[i].left = node[i].right = -1;
	}

	char a, b, c;
	for (int i = 0; i < N; ++i) {
		cin >> a >> b >> c;
		if (b != '.'){
			node[a - 'A'].left = b - 'A';
			node[b - 'A'].parent = a - 'A';
		}

		if (c != '.'){
			node[a - 'A'].right = c - 'A';
			node[c - 'A'].parent = a - 'A';
		}
	}
	preOder(0); cout << '\n';
	inOder(0); cout << '\n';
	postOder(0); cout << '\n';
	return 0;
}