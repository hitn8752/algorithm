#include<iostream>

#define ll long long

using namespace std;

ll tree[1000000 * 4];
int N, M, K;

int init(void) {
	for (int i = 0; i < 4 * N; ++i) tree[i] = 0;
	int tIndex = 1;
	while (tIndex < N) tIndex *= 2;
	tIndex--;
	return tIndex;
}

void modify(int index, ll val) {
	
	ll diff = val - tree[index];
	tree[index] = val;

	index /= 2;
	while (index > 0){
		tree[index] += diff;
		index /= 2;
	}
}

ll intervalSum(int start, int end) {

	ll ret = 0;
	while (start <= end){
		if (start % 2 == 1) ret += tree[start];
		if (end % 2 == 0) ret += tree[end];
		start = (start + 1) / 2;
		end = (end - 1) / 2;
	}
	return ret;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> N >> M >> K;

	int tIndex = init();

	for (int i = 1; i <= N; ++i)
		cin >> tree[tIndex + i];

	for (int i = tIndex; i >= 0; --i)
		tree[i] = tree[i * 2] + tree[i * 2 + 1];

	int a, b, c;
	for (int i = 0; i < M + K; ++i){
		cin >> a >> b >> c;
		
		if (a == 1) {
			modify(tIndex + b, (ll)c);
		}
		else if (a == 2) {
			cout << intervalSum(tIndex + b, tIndex + c) << '\n';
		}
	}
	return 0;
}