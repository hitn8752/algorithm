#include<iostream>
#include<cstring>

using namespace std;

int N;
int queue[10001];
int size;
int f, r;

char str[6];

void push(int x) {
	queue[++r] = x;
	size++;
}

int empty(void) {
	return (size == 0) ? 1 : 0;
}

int pop(void){
	if (empty()) return -1;
	size--;
	return queue[f++];
}

int front(void){
	if (empty()) return -1;
	return queue[f];
}

int back(void) {
	if (empty()) return -1;
	return queue[r];
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> N;
	size = 0;
	f = 0;
	r = -1;
	for (int i = 0; i < N; ++i) {
		cin >> str;
		if (strcmp("push", str) == 0) {
			int a;
			cin >> a;
			push(a);
		}
		else if (strcmp("size", str) == 0)
			cout << size << '\n';
		else if (strcmp("empty", str) == 0)
			cout << empty() << '\n';
		else if (strcmp("pop", str) == 0)
			cout << pop() << '\n';
		else if (strcmp("front", str) == 0)
			cout << front() << '\n';
		else if (strcmp("back", str) == 0)
			cout << back() << '\n';
	}

	return 0;
}