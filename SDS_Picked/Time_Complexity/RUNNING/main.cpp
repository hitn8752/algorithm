#include<iostream>

using namespace std;

typedef struct {
	int index, power;
} node_t;

int N;
node_t P[500001], temp[500001];
int A[500001];

void merge_sort(int left, int right){
	
	if (left == right) return;

	if (right - left == 1){
		if (P[right].power > P[left].power){
			A[P[right].index]--;
			node_t p = P[right];
			P[right] = P[left];
			P[left] = p;
		}
	}
	else {
		int l = left;
		int mid = (left + right) / 2;
		int r = mid + 1;
		merge_sort(l, mid);
		merge_sort(r, right);
		for (int i = left; i <= right; ++i){
			if (l > mid) temp[i] = P[r++];
			else if (r > right) temp[i] = P[l++];
			else if (P[l].power > P[r].power) temp[i] = P[l++];
			else {
				A[P[r].index] -= (mid - l + 1);
				temp[i] = P[r++];
			}
		}
		for (int i = left; i <= right; ++i) P[i] = temp[i];
	}
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> N;
	for (int i = 1; i <= N; ++i){
		cin >> P[i].power;
		P[i].index = A[i] = i;
	}

	merge_sort(1, N);

	for (int i = 1; i <= N; ++i)
		cout << A[i] << '\n';

	return 0;
}