#include<iostream>
#include<string.h>

using namespace std;

int N;
long long memo[91];

long long fibo(int n) {

	if (n <= 1) return n;
	long long& ret = memo[n];
	if (ret != -1) return ret;

	return ret = fibo(n - 1) + fibo(n - 2);
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	memset(memo, -1, sizeof(memo));
	cin >> N;

	cout << fibo(N) << '\n';
	return 0;
}