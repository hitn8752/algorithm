#include<iostream>

using namespace std;

int N, M;
int A[1000001];

int solve(int low, int high, int ret){

	if (low > high) return ret;

	int mid = (low + high) / 2;

	long long sum = 0;
	for (int i = 0; i < N; ++i) {
		int temp = A[i] - mid;
		sum += (temp > 0 ) ? temp : 0;
	}

	if (sum == M) return mid;
	else if (sum > M){
		return solve(mid + 1, high, mid);
	}
	else return solve(low, mid - 1, ret);
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> N >> M;

	int max = 0;
	for (int i = 0; i < N; ++i) {
		cin >> A[i];
		if (A[i] > max) max = A[i];
	}

	int ret = solve(0, max, 0);

	cout << ret << '\n';
		
	return 0;
}