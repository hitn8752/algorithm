#include<iostream>

using namespace std;

int C, N, M;
double cache[1000][2001];

void init(void) {
	cin >> N >> M;

	for (int i = 0; i < 1000; ++i)
		for (int j = 0; j < 2001; ++j)
			cache[i][j] = -1;
}

double climb(int days, int climbed) {
	
	if (days == M) return (climbed >= N) ? 1 : 0;

	double& ret = cache[days][climbed];
	if (ret != -1) return ret;

	return ret = 0.25 * climb(days + 1, climbed + 1) + 0.75 * climb(days + 1, climbed + 2);
}

int main(void) {
	
	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test) {
		init();
		double ret = climb(0, 0);

		printf("%.10lf\n", ret);
	}
	return 0;
}