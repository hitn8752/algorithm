#include<iostream>
#include<algorithm>

#define MAX (1000*1000*1000+1)
using namespace std;

inline int sqr(int a){
	int ret = 1;
	while (ret < a) ret *= 2;
	return ret;
}

int N, M;
int data[1000000*4+1];

int intervalMin(int first, int last){
	int ret = MAX;
	while (first <= last){
		if (first % 2 == 1) ret = min(ret, data[first]);
		if (last % 2 == 0) ret = min(ret, data[last]);
		first = (first + 1) / 2;
		last = (last - 1) / 2;
	}
	return ret;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> M;
	int index = sqr(N);

	for (int i = 0; i < 4 * N; ++i) data[i] = MAX;
	for (int i = 0; i < N; ++i)
		cin >> data[index + i];
	
	for (int i = index - 1; i >= 1; --i)
		data[i] = min(data[i * 2], data[i * 2 + 1]);

	for (int i = 0; i < M; ++i){
		int a, b;
		cin >> a >> b;
		cout << intervalMin(index + a - 1, index + b - 1) << '\n';
	}
	return 0;
}