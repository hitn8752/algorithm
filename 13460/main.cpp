#include<iostream>
#include<vector>
#include<algorithm>

#define MAX 987654321

using namespace std;

typedef struct cord{
	int r, c;
	char color;
	bool operator<(cord b) {
		return c < b.c;
	}
}cord;

typedef struct Board{
	int N, M;
	char board[10][10];
	vector<cord> ball;

	Board(){
		for (int i = 0; i < 10; ++i)
			for (int j = 0; j < 10; ++j)
				board[i][j] = 0;
	}
	Board(int n, int m){
		for (int i = 0; i < 10; ++i)
			for (int j = 0; j < 10; ++j)
				board[i][j] = 0;
		N = n; M = m;
	}
	char* operator[](int index){
		return board[index];
	}
	void swapNM(void){
		int temp = N;
		N = M;
		M = temp;
	}
	void rotate(void){
		ball.clear();
		char temp[10][10];
		for (int i = 0; i < 10; ++i)
			for (int j = 0; j < 10; ++j)
				temp[i][j] = 0;

		for (int i = 0; i < N; ++i){
			for (int j = 0; j < M; ++j){
				temp[j][N - i - 1] = board[i][j];
			}
		}
		swap(N, M);
		for (int i = 0; i < 10; ++i){
			for (int j = 0; j < 10; ++j){
				board[i][j] = temp[i][j];
				if (board[i][j] == 'R' || board[i][j] == 'B')
					ball.push_back({ i, j, board[i][j] });
			}
		}
		
	}
	bool isRightDir(int r, int c){
		return r >= 0 && r < N && c >= 0 && c < M;
	}
	bool move(cord& point){
		bool ret = false;
		while (true){
			int nr = point.r;
			int nc = point.c - 1;

			if (isRightDir(nr, nc) && board[nr][nc] == '.'){
				board[nr][nc] = point.color;
				board[point.r][point.c] = '.';
				point = { nr, nc, point.color };
			}
			else if (isRightDir(nr, nc) && board[nr][nc] == 'O'){
				board[point.r][point.c] = '.';
				ret = true;
				break;
			}
			else break;
		}
		return ret;
	}
	bool L(void){
		vector<int> exit;
		sort(ball.begin(), ball.end());

		for (int i = 0; i < ball.size(); ++i){
			cord temp = ball[i];
			if (move(temp)) exit.push_back(i);
			if (temp.r == ball[i].r && temp.c == ball[i].c)
				return true;
		}

		if (exit.size() == 2)
			ball.clear();
		else if (exit.size() == 1)
			ball.erase(ball.begin() + exit[0]);

		return false;
	}
}Board;

int N, M;
int dfs(Board board, int cnt){

	if (cnt > 10 || board.ball.empty()) return MAX;
	if (board.ball.size() == 1 && board.ball[0].color == 'R')
		return MAX;
	if (board.ball.size() == 1 && board.ball[0].color == 'B')
		return cnt;
	
	int ret = MAX;
	for (int i = 0; i < 4; ++i){
		Board temp = board;
		if(!temp.L())
			ret = min(ret, dfs(temp, cnt + 1));
		board.rotate();
	}

	return ret;
}




int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	
	cin >> N >> M;
	Board board = Board(N, M);
	for (int i = 0; i < N; ++i){
		char str[11]; cin >> str;
		for (int j = 0; j < M; ++j){
			board[i][j] = str[j];
			if (board[i][j] == 'R' || board[i][j] == 'B') board.ball.push_back({ i, j, board[i][j] });
			
		}
	}

	int result = dfs(board, 0);
	if (result == MAX)
		cout << "-1\n";
	else
		cout << result << '\n';
	return 0;
}