#include<iostream>
#include<queue>
using namespace std;

typedef long long ll;
typedef struct Node{
	int left, right;
	ll weight;
	Node(){}
	Node(int _left, int _right, ll _weight) : left(_left), right(_right),
		weight(_weight) {}
	bool operator<(const Node& a) const{
		return weight > a.weight;
	}
}Node;

priority_queue<Node> pq;
int disjoint[10001];
int V, E;

int find(int node){
	if (disjoint[node] == node) return node;
	return disjoint[node] = find(disjoint[node]);
}

void join(int a, int b){
	int pa = find(a);
	int pb = find(b);
	disjoint[pb] = pa;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> V >> E;
	while (!pq.empty()) pq.pop();
	for (int i = 0; i < V; ++i) disjoint[i] = i;
	for (int i = 0; i < E; ++i){
		int a, b; ll c;
		cin >> a >> b >> c;
		pq.push(Node(a, b, c));
	}

	int ret = 0;
	while (!pq.empty()){
		Node node = pq.top(); pq.pop();
		
		int pa = find(node.left);
		int pb = find(node.right);
		if (pa != pb){
			ret += node.weight;
			join(pa, pb);
		}
	}
	cout << ret << '\n';
	return 0;
}