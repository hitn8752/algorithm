#include<iostream>
#include<vector>

using namespace std;

typedef struct TreeNode {
	TreeNode *left, *right;
	int value;
	TreeNode(){}
	TreeNode(int value) : left(NULL), right(NULL), value(value){}
	~TreeNode(){
		delete left;
		delete right;
	}
};

int find(const vector<int> &in, int value){
	for (int i = 0; i < in.size(); ++i){
		if (in[i] == value)
			return i;
	}
	return -1;
}

void makeTree(const vector<int> &pre, const vector<int> &in, TreeNode* &root){
	
	if (pre.size() <= 0) return;

	root = new TreeNode(pre[0]);

	int l = find(in, root->value);
	int r = pre.size() - l - 1;

	makeTree(vector<int>(pre.begin() + 1, pre.begin() + l + 1),
		vector<int>(in.begin(), in.begin() + l), root->left);
	makeTree(vector<int>(pre.begin() + l + 1, pre.end()),
		vector<int>(in.begin() + l + 1, in.end()), root->right);
}

void postOrder(TreeNode* root) {
	
	if (root == NULL) return;

	postOrder(root->left);
	postOrder(root->right);

	cout << root->value << " ";
}

int C, N;
int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 1; test <= C; ++test){
		cin >> N;
		vector<int> pre(N, 0);
		vector<int> in(N, 0);
		TreeNode *root = NULL;

		for (int i = 0; i < N; ++i){
			int a; cin >> a;
			pre[i] = a;
		}

		for (int i = 0; i < N; ++i){
			int a; cin >> a;
			in[i] = a;
		}

		makeTree(pre, in, root);

		postOrder(root);
		cout << '\n';
		delete root;
	}

	return 0;
}
