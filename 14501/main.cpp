#include<iostream>
#include<algorithm>

using namespace std;

int N;
int table[16][2];

int solve(int start, int sum){

	if (start > N)
		return sum;

	int ret = 0;
	for (int i = start; i <= N; ++i){
		if (table[i][0] + i > N + 1)
			ret = max(ret, sum);
		else
			ret = max(ret, solve(table[i][0] + i, sum + table[i][1]));
	}

	return ret;
}
int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N;
	for (int i = 1; i <= N; ++i)
		cin >> table[i][0] >> table[i][1];

	cout << solve(1, 0) << '\n';
	return 0;
}