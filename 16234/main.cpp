#include<iostream>
#include<vector>

using namespace std;

typedef struct Node{
	int sum, cnt;
	Node() : sum(0), cnt(0){}
	Node(int _sum, int _cnt) : sum(_sum), cnt(_cnt){}
	const Node operator+(const Node& a)const{
		Node ret;
		ret.sum = this->sum + a.sum;
		ret.cnt = this->cnt + a.cnt;
		return ret;
	}
}Node;

// up right down left
int dir[5][2] = { { 0, 0 }, { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };
int A[50][50][5];
bool visited1[50][50];
bool visited2[50][50];
int N, L, R;

bool isRightDir(int r, int c){
	return r >= 0 && r < N && c >= 0 && c < N;
}

bool canMove(int a, int b){
	int diff = a - b;
	if (diff < 0) diff *= -1;
	return diff >= L && diff <= R;
}

bool openLine(void){
	bool ret = false;
	for (int r = 0; r < N; ++r)
		for (int c = 0; c < N; ++c)
			for (int d = 1; d < 5; ++d){
				int nr = r + dir[d][0];
				int nc = c + dir[d][1];
				if (isRightDir(nr, nc) 
					&& canMove(A[r][c][0], A[nr][nc][0])){
					A[r][c][d] = 1;
					ret = true;
				}
			}
	return ret;
}

Node dfs1(int r, int c){
	
	visited1[r][c] = true;
	Node ret = {A[r][c][0], 1};
	for (int d = 1; d < 5; ++d){
		int nr = r + dir[d][0];
		int nc = c + dir[d][1];
		if (isRightDir(nr, nc) && !visited1[nr][nc] && A[r][c][d])
			ret = ret + dfs1(nr, nc);
	}
	return ret;
}

void dfs2(int r, int c, int num){
	visited2[r][c] = true;
	A[r][c][0] = num;
	for (int d = 1; d < 5; ++d){
		int nr = r + dir[d][0];
		int nc = c + dir[d][1];
		if (isRightDir(nr, nc) && !visited2[nr][nc] && A[r][c][d])
			dfs2(nr, nc, num);
	}

	for (int d = 1; d < 5; ++d) A[r][c][d] = 0;
}

void movePeople(void){
	
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j){
			visited1[i][j] = false;
			visited2[i][j] = false;
		}

	for (int r = 0; r < N; ++r){
		for (int c = 0; c < N; ++c){
			if (!visited1[r][c]){
				Node node = dfs1(r, c);
				int peopleAfterMove = node.sum / node.cnt;
				dfs2(r, c, peopleAfterMove);
			}
		}
	}
}

int main(void){
	
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> L >> R;
	
	for (int i = 0; i < N; ++i){
		for (int j = 0; j < N; ++j){
			cin >> A[i][j][0];
			visited1[i][j] = false;
			visited2[i][j] = false;
			for (int k = 1; k < 5; ++k) A[i][j][k] = 0;
		}
	}

	int howManyMove = 0;
	while (true) {
		if (!openLine()) break;
		movePeople();
		howManyMove++;
	}	
	cout << howManyMove << '\n';

	return 0;
}