#include<iostream>
#include<map>
#include<deque>

using namespace std;

typedef struct {
	int r, c;
}cord;

typedef struct Snake{
	deque<cord> body;
	int d;
	Snake() : d(1){};
	void move_head(int nr, int nc){
		body.push_back({ nr, nc });
	}
	void remove_tail(void){
		body.pop_front();
	}
	cord head(void){
		return body.back();
	}
	cord tail(void){
		return body.front();
	}
	void L(void){
		if (--d < 0) d = 3;
	}
	void D(void){
		if (++d > 3) d = 0;
	}
}Snake;

int N, K, L, Time;
int dir[4][2] = { {-1,0}, {0,1}, {1,0}, {0,-1} };
int crush[101][101];
int apple[101][101];
Snake s;
map<int, char> m;

bool isRightDir(int r, int c){
	return r > 0 && r <= N && c > 0 && c <= N;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> K;
	for (int i = 0; i < K; ++i){
		int a, b; cin >> a >> b;
		apple[a][b] = 1;
	}

	cin >> L;
	for (int i = 0; i < L; ++i){
		int a; char b; cin >> a >> b;
		m[a] = b;
	}
	
	s.move_head(1, 1);
	crush[1][1] = 1;
	while (true){
		Time++;
		int nr = s.head().r + dir[s.d][0];
		int nc = s.head().c + dir[s.d][1];

		if (!isRightDir(nr, nc) || crush[nr][nc]) break;

		s.move_head(nr, nc);
		crush[nr][nc] = 1;
		if (apple[nr][nc] != 1){
			crush[s.tail().r][s.tail().c] = 0;
			s.remove_tail();
		}else 
			apple[nr][nc] = 0;
		if (m[Time] == 'D') s.D();
		else if (m[Time] == 'L') s.L();
	}

	cout << Time << '\n';
	return 0;
}
