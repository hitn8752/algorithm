#include<iostream>
#include<algorithm>
#include<queue>

using namespace std;

typedef struct Plate{
	int plate[5][5];
	int* operator[](int i){
		return plate[i];
	}
	void rotate(){
		int temp[5][5];
		for (int i = 0; i < 5; ++i)
			for (int j = 0; j < 5; ++j)
				temp[j][4 - i] = plate[i][j];

		for (int i = 0; i < 5; ++i)
			for (int j = 0; j < 5; ++j)
				plate[i][j] = temp[i][j];
	}
}Plate;

Plate p[5];
int result;
int dir[7][3] = {
	{ 0, 0, 0 },
	{ 0, -1, 0 },
	{ 0, 0 ,1 },
	{ 0, 1, 0 },
	{ 0, 0, -1 },
	{ -1, 0, 0 },
	{ 1, 0, 0 }};
bool visited[5][5][5];

void init(void){
	for (int i = 0; i < 5; ++i){
		for (int j = 0; j < 5; ++j){
			for (int k = 0; k < 5; ++k){
				cin >> p[i][j][k];
				visited[i][j][k] = false;
			}
		}
	}
}

typedef struct Node{
	int i, j, k, depth;
	Node(){}
	Node(int _i, int _j, int _k, int _depth) : i(_i), j(_j), k(_k), depth(_depth){}
}Node;

bool isRightDir(int i, int j, int k){
	if (i >= 0 && i < 5 &&
		j >= 0 && j < 5 &&
		k >= 0 && k < 5)
		return true;
	return false;
}

void simulate(void){
	if (!p[0][0][0]) return;

	queue<Node> q;

	visited[0][0][0] = true;
	q.push({ 0, 0, 0, 0 });

	while (!q.empty()){
		Node cur = q.front(); q.pop();
		if (cur.i == 4 && cur.j == 4 && cur.k == 4) {
			result = min(result, cur.depth);
			break;
		}

		for (int i = 1; i < 7; ++i){
			int n_i = cur.i + dir[i][0];
			int n_j = cur.j + dir[i][1];
			int n_k = cur.k + dir[i][2];
			if (isRightDir(n_i, n_j, n_k) && !visited[n_i][n_j][n_k] && p[n_i][n_j][n_k]){
				visited[n_i][n_j][n_k] = true;
				q.push({ n_i, n_j, n_k, cur.depth + 1});
			}
		}
	}
	
	for (int i = 0; i < 5; ++i)
		for (int j = 0; j < 5; ++j)
			for (int k = 0; k < 5; ++k)
				visited[i][j][k] = false;

}

void permutation(int n, int r){
	if (r == 0) {
		simulate();
		return;
	}

	for (int i = n - 1; i >= 0; --i){
		swap(p[i], p[n - 1]);
		permutation(n - 1, r - 1);
		swap(p[i], p[n - 1]);
	}
}

void solve(int index){
	if (index > 4){
		permutation(5, 5);
		return;
	}

	for (int i = 0; i < 4; ++i){
		p[index].rotate();
		solve(index + 1);
	}
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);


	init();
	
	result = 987654321;

	solve(0);

	if (result == 987654321)
		cout << "-1\n";
	else
		cout << result << '\n';


	return 0;
}