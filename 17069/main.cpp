#include<iostream>

#define HORIZONTAL 0
#define VERTICAL 1
#define DIAGONAL 2

using namespace std;

int N;
int map[32][32];
long long dp[32][32][3];

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N;
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			cin >> map[i][j];

	for (int i = 1; i < N && map[0][i] != 1; ++i)
		dp[0][i][HORIZONTAL] = 1;

	for (int i = 1; i < N; ++i){
		for (int j = 1; j < N; ++j){
			dp[i][j][HORIZONTAL] = (map[i][j] == 0) ? dp[i][j - 1][HORIZONTAL]
				+ dp[i][j - 1][DIAGONAL] : 0;
			dp[i][j][VERTICAL] = (map[i][j] == 0) ? dp[i - 1][j][VERTICAL]
				+ dp[i - 1][j][DIAGONAL] : 0;
			dp[i][j][DIAGONAL] = (map[i][j] == 0 && map[i-1][j] == 0 && map[i][j-1] == 0) ? dp[i - 1][j - 1][DIAGONAL]
				+ dp[i - 1][j - 1][HORIZONTAL] + dp[i - 1][j - 1][VERTICAL] : 0;
		}
	}

	cout << dp[N - 1][N - 1][HORIZONTAL] + dp[N-1][N-1][VERTICAL] + dp[N-1][N-1][DIAGONAL] << '\n';
	return 0;
}
