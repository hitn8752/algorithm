#include<iostream>

#define MAX_SIZE 100

using namespace std;

int T, N;
int board[MAX_SIZE][MAX_SIZE];
int cache[MAX_SIZE][MAX_SIZE];

void init(void) {

	cin >> N;
	int temp;

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j){
			cin >> temp;
			board[i][j] = temp;
			cache[i][j] = -1;
		}
}

int solve(int x, int y) {
	
	if (x >= N || y >= N || x < 0 || y < 0)
		return false;

	if (x == N - 1 && y == N - 1)
		return true;

	int& ref = cache[y][x];
	
	if (ref != -1) return ref;

	int jump = board[y][x];

	ref = (solve(x + jump, y) || solve(x, y + jump));

	return ref;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> T;

	for (int i = 0; i < T; ++i) {
		init();

		bool result = solve(0, 0);

		if (result)
			cout << "YES" << '\n';
		else
			cout << "NO" << '\n';
	}

	return 0;
}
