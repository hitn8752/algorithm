#include<iostream>

using namespace std;

typedef struct Cord{
	int r, c;
	Cord(int _r, int _c) : r(_r), c(_c){}
	Cord operator-(Cord& a){
		return Cord(r - a.r, c - a.c);
	}
}Cord;

typedef struct Pipe{
	Cord head, tail;
	Pipe(Cord _head, Cord _tail) : head(_head), tail(_tail){}
}Pipe;

int N, ans;
int map[17][17];

bool isRightDir(int r, int c){
	return r > 0 && r <= N && c > 0 && c <= N;
}

bool hasDiagonalSpace(int r, int c){
	int dir[3][2] = { { 0, 1 }, { 1, 1 }, { 1, 0 } };
	for (int i = 0; i < 3; ++i){
		int nr = r + dir[i][0];
		int nc = c + dir[i][1];
		if (!isRightDir(nr, nc) || map[nr][nc] == 1){
			return false;
		}
	}
	return true;
}

void dfs(Pipe p){
	if (p.head.r == N && p.head.c == N){
		ans++;
		return;
	}

	Cord diff = p.head - p.tail;
	int r = p.head.r;
	int c = p.head.c;

	if (diff.r + diff.c == 2){ //대각선모양
		if (hasDiagonalSpace(r, c)) //직진
			dfs(Pipe({ r + 1, c + 1 }, p.head));

		if (isRightDir(r, c + 1) && map[r][c + 1] == 0) //반시계 회전
			dfs(Pipe({ r, c + 1 }, p.head));

		if (isRightDir(r + 1, c) && map[r + 1][c] == 0) //시계 회전
			dfs(Pipe({ r + 1, c }, p.head));
	}
	else { //가로나 세로모양
		if (isRightDir(r + diff.r, c + diff.c) && map[r + diff.r][c + diff.c] == 0) //직진
			dfs(Pipe({ r + diff.r, c + diff.c }, p.head));
		if (hasDiagonalSpace(r, c)) //회전
			dfs(Pipe({ r + 1, c + 1 }, p.head));
	}
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N;
	for (int i =1; i <= N; ++i)
		for (int j = 1; j <= N; ++j)
			cin >> map[i][j];

	Pipe p = Pipe({ 1, 2 }, { 1, 1 });
	
	dfs(p);

	cout << ans << '\n';
	return 0;
}