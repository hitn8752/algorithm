#include<iostream>
#include<queue>

using namespace std;

int N, M;
queue<int> q;

void josep(void){

	while (q.size() > 1){
		for (int i = 0; i < M - 1; ++i){
			int cur = q.front(); q.pop();
			q.push(cur);
		}
		int cur = q.front(); q.pop();
		cout << cur << ", ";
	}

	cout << q.front();
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> M;

	for (int i = 1; i <= N; ++i)
		q.push(i);

	cout << "<";
	josep();
	cout << ">\n";
	return 0;
}