#include<iostream>

using namespace std;

typedef struct Gear{
	int pole[8];
	int left, right;
	Gear(){}
	Gear(const char str[9]){
		for (int i = 0; i < 8; ++i){
			pole[i] = str[i] - '0';
		}
		left = 6; right = 2;
	}
	int leftPole(void){
		return pole[left];
	}
	int rightPole(void){
		return pole[right];
	}
	int upPole(void){
		int index = (left + 2) % 8;
		return pole[index];
	}
}Gear;

char str[9];
Gear gear[5];
int K;
bool visited[5];
void rotate(int a, int b){
	if (b == 1){
		gear[a].left--; if (gear[a].left < 0) gear[a].left = 7;
		gear[a].right--; if (gear[a].right < 0) gear[a].right = 7;

	} else{
		gear[a].left++; if (gear[a].left > 7) gear[a].left = 0;
		gear[a].right++; if (gear[a].right > 7) gear[a].right = 0;
	}
}

void simulate(int index, int dir){
	if (index > 4 || index < 0) return;
	visited[index] = true;
	int leftside = (index - 1 > 0) ? index - 1 : 0;
	int rightside = (index + 1 < 5) ? index + 1 : 0;
	if (leftside != 0 && !visited[leftside] && gear[leftside].rightPole() != gear[index].leftPole()) 
		simulate(index - 1, -1 * dir);
	if (rightside != 0 && !visited[rightside] && gear[rightside].leftPole() != gear[index].rightPole())
		simulate(index + 1, -1 * dir);
	rotate(index, dir);
}
int sqr(int a, int b){
	int ret = 1;
	for (int i = 0; i < b; ++i){
		ret = ret * a;
	}
	return ret;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	for (int i = 1; i < 5; ++i){
		cin >> str;
		gear[i] = Gear(str);
	}

	cin >> K;
	for (int i = 0; i < K; ++i){
		int a, b;
		cin >> a >> b;
		for (int j = 0; j < 5; ++j) visited[j] = false;
		simulate(a, b);
	}

	int result = 0;
	for (int i = 1; i < 5; ++i)
		if (gear[i].upPole()) result += sqr(2, i - 1);
	
	cout << result << '\n';

	return 0;
}