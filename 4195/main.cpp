#include<iostream>
#include<string>
#include<map>

using namespace std;

typedef struct Node{
	string name;
	int num;
	Node(){}
	Node(string _name, int _num) : name(_name), num(_num){}
}Node;

int s[200001];
int n[200001];
int T, F, order;
map<string, int> m;

int find(int a){
	if (s[a] == a) return a;
	return s[a] = find(s[a]);
}

void join(int a, int b){
	int pa = find(a);
	int pb = find(b);
	s[pb] = pa;
	if (pa != pb)
		n[pa] += n[pb];
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> T;
	for (int test = 1; test <= T; ++test){
		m.clear();
		for (int i = 0; i < 200001; ++i) {
			s[i] = i;
			n[i] = 1;
		}
		order = 0;
		cin >> F;
		cin.ignore();
		for (int i = 0; i < F; ++i){
			string str;
			getline(cin, str);
			int split = 0;
			while (str[split] != ' ') split++;
			string a = str.substr(0, split);
			string b = str.substr(split + 1);
			int na = m[a] == 0 ? ++order : m[a];
			int nb = m[b] == 0 ? ++order : m[b];
			m[a] = na;
			m[b] = nb;
			join(na, nb);
			int pa = find(na);
			cout << n[pa] << '\n';
		}
	}
	return 0;
}