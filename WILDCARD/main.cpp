#include<iostream>
#include<string>
#include<string.h>
#include<queue>

using namespace std;

typedef struct {
	bool operator()(string a, string b) {
		return a > b;
	}
}cmp;

int C, N;
string W, F;
int cache[101][101];
priority_queue<string, vector<string>, cmp> pq;

bool match(int w, int s) {

	int& ret = cache[w][s];
	if (ret != -1) return ret;

	while (w < W.size() && s < F.size()
		&& (W[w] == '?' || W[w] == F[s])){
		++w;
		++s;
	}

	if (w == W.size())
		return ret = (s == F.size());

	if (W[w] == '*') {
		for (int skip = 0; s + skip <= F.size(); ++skip)
			if (match(w + 1, skip + s))
				return ret = 1;
	}

	return ret = 0;
}

int main(void) {
	
	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int i = 0; i < C; ++i) {
		cin >> W >> N;
		for (int j = 0; j < N; ++j) {
			memset(cache, -1, sizeof(cache));
			cin >> F;
			if (match(0, 0))
				pq.push(F);
		}

		while (!pq.empty()) {
			cout << pq.top() << '\n';
			pq.pop();
		}
	}
	
	return 0;
}