#include<iostream>
#include<vector>

using namespace std;

int C, N, K;
vector<int> v; 
int main(void){
	
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 1; test <= C; ++test){
		cin >> N >> K;

		v.clear();
		for (int i = 0; i <= N; ++i)
			v.push_back(i);

		vector<int>::iterator it = v.begin() + 1;
		
		while (N > 2){
			it = v.erase(it);
			if (it == v.end()) it = v.begin() + 1;
			N--;
			for (int i = 0; i < K - 1; ++i){
				it++;
				if (it == v.end()) it = v.begin() + 1;
			}
		}
		cout << v[1] << " " << v[2] << '\n';
	}

	return 0;
}