#include<iostream>

using namespace std;

int N, K;
int A[10][10];

typedef struct Node{
	int seq[20];
	int size, capacity, iter, win, order;
	Node() : size(0), capacity(20), iter(0), win(0){}
	Node(int _order) : size(0), capacity(20), iter(0), win(0), order(_order){}
	int& operator[](int idx){
		return seq[idx];
	}
	bool isEmpty(void){
		return size == 0;
	}
	bool isFull(void){
		return size == capacity;
	}
	void push_back(int a){
		if (isFull()) return;
		seq[size++] = a;
	}
	void pop_back(void){
		if (isEmpty()) return;
		seq[--size] = 0;
	}
	int next(void){
		if (iter == size) return -1;
		return seq[iter++];
	}
	bool hasNext(void){
		return iter != size;
	}
	void clear_iter(void){
		iter = 0;
		win = 0;
	}
	int fight(Node& other){
		int a = next();
		int b = other.next();
		if (a == -1 || b == -1) return -1;
		if (A[a][b] == 2){
			++win;
			return 1;
		}
		else if (A[a][b] == 0) {
			++(other.win);
			return 0;
		}
		else {
			if (order > other.order){
				++win;
				return 1;
			}
			else{
				++(other.win);
				return 0;
			}
		}
	}
};

Node P[3];

void init(void){
	cin >> N >> K;
	for (int i = 1; i <= N; ++i){
		for (int j = 1; j <= N; ++j){
			cin >> A[i][j];
		}
		P[0].push_back(i);
	}

	for (int i = 1; i <= 2; ++i){
		for (int j = 0; j < 20; ++j){
			int a;
			cin >> a;
			P[i].push_back(a);
		}
	}

	for (int i = 0; i < 3; ++i)
		P[i].order = i;
}

void swap(Node* &a, Node* &b){
	Node* temp = a;
	a = b;
	b = temp;
}

bool simulate(void){
	Node* first = &P[0];
	Node* second = &P[1];
	Node* third = &P[2];
	bool ret = false;

	while (true){
		if (P[0].win == K) {
			ret = true;
			break;
		}
		if (P[1].win == K || P[2].win == K || !P[2].hasNext()) break;
		int result = first->fight(*second);
		if (result == 1) swap(second, third);
		else {
			swap(second, first);
			swap(second, third);
		}
	}

	P[0].clear_iter();
	P[1].clear_iter();
	P[2].clear_iter();

	return ret;
}

void swap(int& a, int &b){
	int temp = a;
	a = b;
	b = temp;
}

bool solve(int n, int r){
	if (r == 0){
		return simulate();
	}

	for (int i = n - 1; i >= 0; --i){
		swap(P[0][i], P[0][n - 1]);
		if (solve(n - 1, r - 1)){
			return true;
		}
		swap(P[0][i], P[0][n - 1]);
	}

	return false;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	init();

	cout << solve(N, N) << '\n';

	return 0;
}