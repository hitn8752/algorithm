#include<iostream>

using namespace std;

int a[1000];

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	int n, m, l, pass, cur;
	cin >> n >> m >> l;

	cur = 0;
	pass = 0;
	while (true){
		if (++a[cur] == m) break;
		if (a[cur] % 2 != 0)
			cur = cur - l < 0 ? n - (l - cur) : cur - l;
		else
			cur = cur + l >= n ? l - (n - cur) : cur + l;
		pass++;
	}
	cout << pass << '\n';

	return 0;
}