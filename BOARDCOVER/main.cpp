//
//  main.cpp
//  BOARDCOVER
//
//  Created by Hwang on 2018. 11. 17..
//  Copyright © 2018년 Hwang. All rights reserved.
//

#include <iostream>
#include <string>

using namespace std;

int t, h, w;
short map[21][21];
short blockType[4][3][2] = {
    { {0,0}, {0,1}, {1,0} },
    { {0,0}, {0,1}, {1,1} },
    { {0,0}, {1,0}, {1,1} },
    { {0,0}, {1,0}, {1,-1} }
};

void init() {
    
    cin >> h >> w;
    cin.ignore();
    
    string s;
    
    for(int i = 0; i < h; ++i) {
        cin >> s;
        for(int j = 0; j < w; ++j) {
            if(s[j] == '#') map[i][j] = 1;
            else map[i][j] = 0;
        }
    }
}

bool putBlock(int x, int y, int type, int op) {
    
    bool ok = true;
    
    for(int i = 0; i < 3; ++i) {
        int dx = x + blockType[type][i][1];
        int dy = y + blockType[type][i][0];
        
        if(dx < 0 || dy < 0 || dx >= w || dy >= h) ok = false;
        else if((map[dy][dx] += op) > 1) ok = false;;
    }
    
    return ok;
}

int countPutting() {
    
    int dx = -1, dy = -1;
    
    for(int i = 0; i < h; ++i) {
        for(int j = 0; j < w; ++j) {
            if(map[i][j] == 0) {
                dy = i;
                dx = j;
                break;
            }
        }
        if(dy != -1) break;
    }
    
    if(dy == -1) return 1;
    
    int ret = 0;
    for(int d = 0; d < 4; ++d) {
        if(putBlock(dx, dy, d, 1)) {
            ret += countPutting();
        }
        
        putBlock(dx, dy, d, -1);
    }
    
    return ret;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    
    freopen("/Users/hwang/Documents/Xcodeworkspace/BOARDCOVER/BOARDCOVER/input.txt", "r", stdin);
    
    cin >> t;
    
    for(int test = 0; test < t; ++test) {
        init();

        int ret = countPutting();
        
        cout << ret << endl;
    }
    
    return 0;
}
