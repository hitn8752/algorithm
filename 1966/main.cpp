#include<iostream>
#include<queue>

using namespace std;

typedef struct Node {
	int num, val;
	Node(){}
	Node(int _num, int _val) : num(_num), val(_val) {}
	bool operator<(const Node& a) const {
		return val < a.val;
	}
}Node;

queue<Node> q;
priority_queue<Node> pq;

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	int t;
	cin >> t;
	for (int test = 1; test <= t; ++test){
		while (!q.empty()) q.pop();
		while (!pq.empty()) pq.pop();
		int n, m;
		cin >> n >> m;
		for (int i = 0; i < n; ++i){
			int a;
			cin >> a;
			q.push(Node(i, a));
			pq.push(Node(i, a));
		}

		int cnt = 0;
		while (true){
			Node a = q.front();
			Node b = pq.top();

			if (a.val < b.val){
				q.pop();
				q.push(a);
			}
			else {
				cnt++;
				if (a.num == m) break;
				q.pop();
				pq.pop();
			}
		}
		cout << cnt << '\n';
	}
	return 0;
}