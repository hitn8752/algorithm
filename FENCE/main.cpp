#include<iostream>
#include<vector>

#define MAX(x, y) (x > y) ? x : y
#define MIN(x, y) (x < y) ? x : y

using namespace std;

int T, N;
vector<int> fence;

void init(void) {

	fence.clear();

	cin >> N;

	for (int i = 0; i < N; ++i) {
		int a;
		cin >> a;
		fence.push_back(a);
	}
}

int solve(int left, int right) {

	if (left == right) return fence[left];

	int mid = (left + right) / 2;

	int ret = MAX(solve(left, mid), solve(mid + 1, right));

	int start = mid; int end = mid + 1;

	int height = MIN(fence[start], fence[end]);

	ret = MAX(ret, height * 2);

	while (start > left || end < right) {
		if (end < right && (start == left || fence[start - 1] < fence[end + 1])) {
			++end;
			height = MIN(height, fence[end]);
		}
		else {
			--start;
			height = MIN(height, fence[start]);
		}

		ret = MAX(ret, height * (end - start + 1));
	}

	return ret;
}

int main(void) {
	
	//freopen("./input.txt", "r", stdin);

	cin >> T;

	for (int i = 0; i < T; ++i) {
		init();

		int result = solve(0, fence.size()-1);

		cout << result << endl;
	}
	
	return 0;
}