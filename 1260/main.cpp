#include<iostream>
#include<queue>

using namespace std;

int N, M, V;
int graph[1001][1001];
bool visited[1001];
queue<int> q;

void dfs(int start){
	
	cout << start << " ";
	visited[start] = true;

	for (int i = 1; i <= N; ++i){
		if (graph[start][i] != 0 && !visited[i]){
			dfs(i);
		}
	}
}

void bfs(int start){

	visited[start] = true;
	q.push(start);
	while (!q.empty()){
		int cur = q.front(); q.pop();
		cout << cur << " ";
		for (int i = 1; i <= N; ++i){
			if (graph[cur][i] != 0 && !visited[i]){
				visited[i] = true;
				q.push(i);
			}
		}
	}
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> M >> V;

	for (int i = 1; i <= N; ++i)
		for (int j = 1; j <= M; ++j)
			graph[i][j] = 0;

	for (int i = 0; i < M; ++i){
		int a, b;
		cin >> a >> b;
		graph[a][b] = 1;
		graph[b][a] = 1;
	}

	for (int i = 0; i <= N; ++i) visited[i] = false;
	dfs(V);
	cout << '\n';
	for (int i = 0; i <= N; ++i) visited[i] = false;
	while (!q.empty()) q.pop();
	bfs(V);
	cout << '\n';

	return 0;
}