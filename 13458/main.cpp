#include<iostream>

using namespace std;

int N, B, C;
int A[1000000];
int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N;
	for (int i = 0; i < N; ++i) cin >> A[i];
	cin >> B >> C;


	long long result = 0;

	for (int i = 0; i < N; ++i){
		A[i] -= B;
		result++;
		if (A[i] > 0){
			result = result + (A[i] / C);
			if (A[i] % C != 0) result++;
		}
	}

	cout << result << '\n';

	return 0;
}