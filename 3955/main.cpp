#include<iostream>

using namespace std;

typedef long long ll;
typedef struct RET{
	long long x, y;
	RET(long long _x, long long _y) : x(_x), y(_y) {}
}RET;

int T;

long long gcd(long long a, long long b)
{
	if (b>0) return gcd(b, a%b);
	else return a;
}

RET exgcd(ll a, ll b){

	ll s1 = 1, s2 = 0, s3 = 0;
	ll t1 = 0, t2 = 1, t3 = 1;
	ll r1 = a, r2 = b, r3 = b, q;
	ll g = gcd(a, b);

	while (r3 > g){
		q = r1 / r2;
		r3 = r1%r2;
		s3 = s1 - q * s2;
		t3 = t1 - q * t2;
		s1 = s2, s2 = s3;
		t1 = t2, t2 = t3;
		r1 = r2, r2 = r3;
	}
	return RET(s3, t3);
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//freopen("./input.txt", "r", stdin);

	cin >> T;
	for (int test = 1; test <= T; ++test){
		ll k, c;
		cin >> k >> c;

		if (gcd(k, c) != 1) {
			cout << "IMPOSSIBLE\n";
			continue;
		}

		RET ret = exgcd(k, c);

		long long ans = ret.y, ans2 = ret.x;
		while (ans <= 0 || ans2 >= 0)
		{
			ans += k;
			ans2 -= c;
		}

		if (ans > 1000000000L)
			cout << "IMPOSSIBLE\n";
		else
			cout << ans << '\n';
	}
	return 0;
}