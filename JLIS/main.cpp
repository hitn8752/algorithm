#include<iostream>
#include<string.h>
#include<limits>

#define MAX(x, y) (x > y) ? x : y
#define MIN(x, y) (x < y) ? x : y

using namespace std;

const long long NEGINF = numeric_limits<long long>::min();

int A[100], B[100];
int C, N, M;
int cache[101][101];

int solve(int startA, int startB) {

	if (startA >= N || startB >= M) return 0;

	int& ret = cache[startA + 1][startB + 1];
	if (ret != -1) return ret;

	ret = 0;
	long long a = (startA == -1 ? NEGINF : A[startA]);
	long long b = (startB == -1 ? NEGINF : B[startB]);
	long long maxElement = MAX(a, b);

	for (int nextA = startA + 1; nextA < N; ++nextA)
		if (A[nextA] > maxElement){
			int temp = solve(nextA, startB) + 1;
			ret = MAX(ret, temp);
		}

	for (int nextB = startB + 1; nextB < M; ++nextB) 
		if (B[nextB] > maxElement){
			int temp = solve(startA, nextB) + 1;
			ret = MAX(ret, temp);
		}

	return ret;
}

int main(void) {
	
	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test) {
		memset(cache, -1, sizeof(cache));

		cin >> N >> M;

		int a;
		for (int i = 0; i < N; ++i) {
			cin >> a;
			A[i] = a;
			//cout << A[i] << " ";
		}
		//cout << endl;

		for (int i = 0; i < M; ++i) {
			cin >> a;
			B[i] = a;
			//cout << B[i] << " ";
		}
		//cout << endl;

		int ret = solve(-1, -1);

		cout << ret << '\n';
	}

	return 0;
}