#include<iostream>
#include<algorithm>
#include<string.h>

#define INF 987654321

using namespace std;

int C, N, S;
int A[101], pSum[101], pSqSum[101];
int cache[101][11];

void init(void) {
	cin >> N;
	cin >> S;

	int a;
	for (int i = 0; i < N; ++i) {
		cin >> a;
		A[i] = a;
		//cout << A[i] << " ";
	}
	//cout << endl;

	memset(cache, -1, sizeof(cache));
}

void preprocess(void) {

	sort(A, A + N);

	pSum[0] = A[0];
	pSqSum[0] = A[0] * A[0];
	for (int i = 1; i < N; ++i){
		pSum[i] = A[i] + pSum[i - 1];
		pSqSum[i] = (A[i] * A[i]) + pSqSum[i - 1];
	}

	/*for (int i = 0; i < N; ++i)
		cout << A[i] << " ";
	cout << endl;

	for (int i = 0; i < N; ++i)
		cout << pSqSum[i] << " ";
	cout << endl;*/
}

//[start, end]
int minError(int start, int end){
	
	//pSum[b] - pSum[a-1]
	int sum = pSum[end] - (start == 0 ? 0 : pSum[start - 1]);
	//pSqSum[b] - pSqSum[a-1]
	int sqSum = pSqSum[end] - (start == 0 ? 0 : pSqSum[start - 1]);

	int m = int(0.5 + (double)sum / (end - start + 1));
	
	int ret = sqSum - (2 * m * sum) + (m * m * (end - start + 1));
	
	return ret;
}

int quantizate(int from, int size){

	if (from == N) return 0;

	if (size == 0) return INF;

	int& ret = cache[from][size];
	if (ret != -1) return ret;

	ret = INF;
	for (int i = 1; i <= N - from; ++i){
		int a = minError(from, from + i - 1) + quantizate(from + i, size - 1);
		ret = min(ret, a);
	}
	
	return ret;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> C;

	int ret;
	for (int test = 0; test < C; ++test){
		init();
		preprocess();

		ret = quantizate(0, S);

		cout << ret << '\n';
	}

	return 0;
}
