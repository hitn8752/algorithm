#include<iostream>
#include<stack>
#include<string.h>

using namespace std;

int C, s_len;
char str[10001];
stack<char> s;

bool isOpenning(const char &c){
	if (c == '{' || c == '[' || c == '(')
		return true;
	else return false;
}

bool isPair(const char &a, const char &b) {
	if (a == '(' && b == ')') return true;
	else if (a == '[' && b == ']') return true;
	else if (a == '{' && b == '}') return true;
	else return false;
}

bool solve(char str[]){
	for (int i = 0; i < s_len; ++i){
		if (isOpenning(str[i]))
			s.push(str[i]);
		else {
			if (s.empty())	return false;
			if (!isPair(s.top(), str[i])) return false;
			s.pop();
		}
	}
	return s.empty();
}

int main(void){
	
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("input.txt", "r", stdin);

	cin >> C;
	for (int test = 1; test <= C; ++test){
		cin >> str;
		s_len = strlen(str);
		while (!s.empty()) s.pop();
		bool isMatched = solve(str);
		if (isMatched) cout << "YES\n";
		else cout << "NO\n";
	}
	return 0;
}