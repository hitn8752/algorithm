#include<iostream>
#include<string>

using namespace std;

int T;
string str;

string solve(string::iterator& iter) {

	char head = *iter;
	iter++;

	if (head == 'w' || head == 'b')
		return string(1, head);

	string upperLeft = solve(iter);
	string upperRight = solve(iter);
	string lowerLeft = solve(iter);
	string lowerRight = solve(iter);

	return string("x") + lowerLeft + lowerRight + upperLeft + upperRight;

}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> T;

	for (int i = 0; i < T; ++i){
		cin >> str;
		
		string::iterator iter = str.begin();

		string result = solve(iter);

		cout << result << endl;
	}

	return 0;
}