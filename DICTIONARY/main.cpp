#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int adj[26][26];
bool visited[26];
vector<string> words;
vector<int> order;
char str[21];
int C, N;

void make_graph(const vector<string>& words){

	for (int i = 0; i < words.size() - 1; ++i){
		int j = i + 1, len = min(words[i].size(), words[j].size());

		for (int k = 0; k < len; ++k){
			if (words[i][k] != words[j][k]){
				int a = words[i][k] - 'a';
				int b = words[j][k] - 'a';
				adj[a][b] = 1;
				break;
			}
		}
	}
}

void dfs(int here) {
	visited[here] = 1;
	for (int there = 0; there < 26; ++there){
		if (adj[here][there] && !visited[there]) dfs(there);
	}
	order.push_back(here);
}

vector<int> topology_sort(void){
	for (int i = 0; i < 26; ++i) visited[i] = false;
	
	order.clear();
	for (int i = 0; i < 26; ++i){
		if (!visited[i]) dfs(i);
	}

	reverse(order.begin(), order.end());

	for (int i = 0; i < order.size(); ++i){
		for (int j = i + 1; j < order.size(); ++j){
			if (adj[order[j]][order[i]])
				return vector<int>();
		}
	}

	return order;
}

int main(void) {

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> C;
	for (int test = 1; test <= C; ++test){
		for (int i = 0; i < 26; ++i)
			for (int j = 0; j < 26; ++j)
				adj[i][j] = 0;
		words.clear();

		cin >> N;
		for (int i = 0; i < N; ++i){
			cin >> str;
			words.push_back(str);
		}

		make_graph(words);

		order = topology_sort();

		if (order.size() == 0) cout << "INVALID HYPOTHESIS\n";
		else{
			for (int i = 0; i < order.size(); ++i)
				cout << (char)(order[i] + 'a');
			cout << '\n';
		}
	}
	return 0;
}
