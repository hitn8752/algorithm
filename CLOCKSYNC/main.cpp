//
//  main.cpp
//  CLOCKSYNC
//
//  Created by Hwang on 2018. 11. 24..
//  Copyright © 2018년 Hwang. All rights reserved.
//

#include <iostream>
#include <vector>

#define MIN(x, y) (x < y) ? x : y

using namespace std;

int t;
//vector<int> clocks;
short linked[10][16] = {
    {1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,0},
    {0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,1},
    {1,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,1,1,1,0,1,0,1,0,0,0},
    {1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1},
    {0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1},
    {0,0,0,0,1,1,0,1,0,0,0,0,0,0,1,1},
    {0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,1,1,0,0,0,1,0,0,0,1,0,0}
};

void init(vector<int>& clocks) {
    int n;
    
    for(int i = 0; i < 16; ++i) {
        cin >> n;
        clocks.push_back(n);
    }
}

bool isDone(vector<int>& clocks) {
    
    vector<int>::iterator it;
    
    for(it = clocks.begin(); it != clocks.end(); ++it) {
        if(*it != 12) return false;
    }
    
    return true;
}

void push(vector<int>& clocks, int switchNum) {
    
    for(int i = 0; i < 16; ++i) {
        if(linked[switchNum][i] == 1) {
            clocks[i] = clocks[i] + 3;
            if(clocks[i] == 15) clocks[i] = 3;
        }
    }
}

int solve(vector<int>& clocks, int switchNum) {
    
    if(switchNum == 10) return isDone(clocks) ? 0 : 987654321;
    
    int ret = 987654321;
    for(int i = 0; i < 4; ++i) {
        ret = MIN(ret, i + solve(clocks, switchNum + 1));
        push(clocks, switchNum);
    }
    
    return ret;
}
int main(int argc, const char * argv[]) {
    // insert code here...
    
    freopen("/Users/hwang/Documents/Xcodeworkspace/CLOCKSYNC/CLOCKSYNC/input.txt", "r", stdin);
    
    cin >> t;
    
    for(int test = 1; test <= t; ++test) {
        vector<int> clocks;
        init(clocks);
        int result = solve(clocks, 0);
        
        if(result == 987654321) result = -1;
        
        cout << result << endl;
    }
    
//    for(int i = 0; i < 10; ++i) {
//        for(int j = 0; j < 16; ++j) {
//            if(linked[i][j] == 1) cout << j << " ";
//        }
//        cout << endl;
//    }
    
    return 0;
}
