#include<iostream>
#include<vector>
#include<string>

#define MIN(x, y) (x < y) ? x : y
#define MAX(x, y) (x > y) ? x : y

using namespace std;

int T;
vector<int> members;
vector<int> fans;

vector<int> multiply(const vector<int>& a, const vector<int>& b) {

	vector<int> c(a.size() + b.size() + 1, 0);

	for (int i = 0; i < a.size(); ++i)
		for (int j = 0; j < b.size(); ++j)
			c[i + j] += a[i] * b[j];

	return c;
}

void addTo(vector<int>& a, const vector<int>& b, int k) {
	a.resize(MAX(a.size(), b.size() + k));
	for (int i = 0; i < b.size(); i++) a[i + k] += b[i];
}
void subFrom(vector<int>& a, const vector<int>& b) {
	a.resize(MAX(a.size(), b.size()) + 1);
	for (int i = 0; i < b.size(); i++) a[i] -= b[i];
}

vector<int> karatsuba(const vector<int>& a, const vector<int>& b) {

	if (a.size() < b.size()) return karatsuba(b, a);

	if (a.size() == 0 || b.size() == 0) return vector<int>();

	//if (a.size() == 1 || b.size() == 1) return multiply(a, b);
	if (a.size() <= 50) return multiply(a, b);

	int half = a.size() / 2;
	
	vector<int> a0(a.begin(), a.begin() + half);
	vector<int> a1(a.begin() + half, a.end());
	int tmp = MIN(half, b.size());
	vector<int> b0(b.begin(), b.begin() + tmp);
	vector<int> b1(b.begin() + tmp, b.end());

	vector<int> z0 = karatsuba(a0, b0);

	vector<int> z2 = karatsuba(a1, b1);

	addTo(a0, a1, 0); addTo(b0, b1, 0);

	//(a0 + a1) * (b0 + b1) - z0 - z2
	vector<int> z1 = karatsuba(a0, b0);
	subFrom(z1, z0); subFrom(z1, z2);

	//result = z0 + (z1^half) + (z2^(half*2))
	vector<int> result;
	addTo(result, z0, 0);
	addTo(result, z1, half);
	addTo(result, z2, half * 2);

	return result;
}

void init(void) {

	members.clear();
	fans.clear();

	string mem, fan;

	cin >> mem;
	cin >> fan;

	for (int i = mem.size() - 1; i >= 0; --i)
		members.push_back((mem[i] == 'M'));

	for (int i = 0; i < fan.size(); ++i)
		fans.push_back((fan[i] == 'M'));

}

int main(void){
	
	ios_base::sync_with_stdio(false); cin.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> T;

	for (int i = 0; i < T; ++i) {
		init();

		vector<int> c = karatsuba(members, fans);

		int result = 0;

		for (int i = members.size() - 1; i < fans.size(); ++i)
			if (c[i] == 0) result++;

		cout << result << '\n';
	}

	return 0;
}