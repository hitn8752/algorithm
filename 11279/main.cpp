#include<iostream>
#include<vector>

using namespace std;

typedef struct MAX_Heap{
	vector<int> heap;
	MAX_Heap(){}
	void push_heap(int value){
		heap.push_back(value);

		int index = heap.size() - 1;

		while (index > 0 && heap[(index - 1) / 2] < heap[index]) {
			int temp = heap[index];
			heap[index] = heap[(index - 1) / 2];
			heap[(index - 1) / 2] = temp;
			index = (index - 1) / 2;
		}

	}

	int pop_heap(void) {

		if (heap.empty()) return 0;

		int ret = heap[0];
		heap[0] = heap.back();
		heap.pop_back();

		int index = 0;
		while (true){
			int left = index * 2 + 1, right = index * 2 + 2;

			if (left >= heap.size()) break;

			int next = index;
			if (heap[next] < heap[left]) next = left;
			if (right < heap.size() && heap[next] < heap[right]) next = right;

			if (next == index) break;
			int temp = heap[next];
			heap[next] = heap[index];
			heap[index] = temp;
			index = next;
		}
		return ret;
	}

	bool empty(void){
		return (heap.size() == 0);
	}
}MH;

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, M;
	MAX_Heap hp;

	cin >> N;
	for (int i = 0; i < N; ++i){
		cin >> M;
		if (M == 0) cout << hp.pop_heap() << '\n';
		else hp.push_heap(M);
	}
	return 0;
}
