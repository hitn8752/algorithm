#include<iostream>

using namespace std;

int N, M, H;
int Ladder[31][11];

bool isRight(void){
	for (int i = 1; i <= N; ++i){
		int cur = i;
		for (int j = 1; j <= H; ++j)
			cur = cur + Ladder[j][cur];
		if (cur != i) return false;
	}
	return true;
}

bool dfs(int start, int depth, int limit){
	if (depth == limit){
		if (isRight()) return true;
		else return false;
	}

	for (int h = start; h <= H; ++h){
		for (int i = 1; i < N; ++i){
			if (Ladder[h][i] || Ladder[h][i+1]) continue;
			Ladder[h][i] = 1;
			Ladder[h][i + 1] = -1;
			if (dfs(h, depth + 1, limit)) return true;
			Ladder[h][i] = 0;
			Ladder[h][i + 1] = 0;
		}
	}

	return false;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N >> M >> H;
	for (int i = 0; i < M; ++i){
		int a, b;
		cin >> a >> b;
		Ladder[a][b] = 1;
		Ladder[a][b + 1] = -1;
	}

	for (int i = 0; i <= 3; ++i){
		if (dfs(1, 0, i)) {
			cout << i << '\n';
			return 0;
		}
	}
	cout << "-1\n";

	return 0;
}