#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int N;
int o_board[20][20];

typedef struct Board{
	int board[20][20];
	Board(){
		for (int i = 0; i < N; ++i)
			for (int j = 0; j < N; ++j)
				board[i][j] = o_board[i][j];
	}
	int* operator[](int idx){
		return board[idx];
	}
	void Rotate(void){
		int temp[20][20];
		for (int i = 0; i < N; ++i){
			for (int j = 0; j < N; ++j)
				temp[j][N - i - 1] = board[i][j];
		}
		for (int i = 0; i < N; ++i){
			for (int j = 0; j < N; ++j)
				board[i][j] = temp[i][j];
		}
	}
	void L(void){
		for (int i = 0; i < N; ++i){
			vector<pair<int, bool>> v;
			for (int j = 0; j < N; ++j){
				if (board[i][j] == 0) continue;
				if (!v.empty() && !v.back().second && v.back().first == board[i][j]){
					v[v.size() - 1].first *= 2;
					v[v.size() - 1].second = true;
				}
				else
					v.push_back({ board[i][j], false });
				board[i][j] = 0;
			}
			for (int j = 0; j < v.size(); ++j)
				board[i][j] = v[j].first;
		}
	}
	void R(void){
		Rotate(); Rotate();
		L();
		Rotate(); Rotate();
	}
	void U(void){
		Rotate(); Rotate(); Rotate();
		L();
		Rotate();
	}
	void D(void){
		Rotate();
		L();
		Rotate(); Rotate(); Rotate();	
	}
	int max_value(void){
		int ret = 0;
		for (int i = 0; i < N; ++i){
			for (int j = 0; j < N; ++j){
				ret = max(ret, board[i][j]);
			}
		}
		return ret;
	}
}Board;

int dfs(Board& b, int depth){
	if (depth == 5)
		return b.max_value();

	int copy[20][20];
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			copy[i][j] = b[i][j];
	
	int ret = 0;
	b.L(); ret = max(ret, dfs(b, depth + 1));
	
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			b[i][j] = copy[i][j];

	b.U(); ret = max(ret, dfs(b, depth + 1));

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			b[i][j] = copy[i][j];

	b.R(); ret = max(ret, dfs(b, depth + 1));

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			b[i][j] = copy[i][j];

	b.D(); ret = max(ret, dfs(b, depth + 1));

	return ret;
}

int main(void){

	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	freopen("./input.txt", "r", stdin);

	cin >> N;
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			cin >> o_board[i][j];

	Board b;
	
	int result = dfs(b, 0);

	cout << result << '\n';
	return 0;

}
