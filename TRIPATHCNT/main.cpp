#include<iostream>
#include<string.h>

#define MAX(x, y) (x > y) ? x : y

using namespace std;

int C, N;
int triangle[100][100];
int cache[100][100];
int cacheCnt[100][100];

void init(void) {
	memset(cache, -1, sizeof(cache));
	memset(cacheCnt, -1, sizeof(cacheCnt));
	cin >> N;

	int a;
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j <= i; ++j){
			cin >> a;
			triangle[i][j] = a;
			//cout << triangle[i][j] << " ";
		}
		//cout << endl;
	}
			

}

int path(int x, int y) {

	int& ret = cache[y][x];
	if (y == N - 1) return ret = triangle[y][x];

	if (ret != -1) return ret;

	int a = path(x, y + 1);
	int b = path(x + 1, y + 1);
	return ret = triangle[y][x] + (MAX(a, b));

}

int pathCnt(int x, int y) {

	if (y == N - 1) return 1;

	int& ret = cacheCnt[y][x];
	if (ret != -1) return ret;

	ret = 0;
	if (cache[y + 1][x] >= cache[y + 1][x + 1])  ret += pathCnt(x, y + 1);
	if (cache[y + 1][x] <= cache[y + 1][x + 1])  ret += pathCnt(x + 1, y + 1);
	return ret;
}

int main(void) {

	freopen("./input.txt", "r", stdin);

	cin >> C;

	for (int test = 0; test < C; ++test) {
		init();

		path(0, 0);

		int ret = pathCnt(0, 0);

		cout << ret << '\n';
	}
	return 0;
}